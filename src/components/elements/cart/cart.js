import React, {useState} from 'react';
import Image from 'next/image';
import styles from './index.module.css'
import Modal from 'react-modal'
import { useCart, useCartDispatch, useCartTotal } from '@/context/CartContext';
import api from '@/api';

const Cart = () => {
  const carts = useCart();
  const dispatch = useCartDispatch();
  const total = useCartTotal();
  const [isOpen, setIsOpen] = useState(false);
  const [totalValue, setTotal] = useState(0);
  const [noOrder, setNoOrder] = useState('');
  const [payAmount, setPayAmount] = useState(0);
  const [change, setChange] = useState(0);
  // const [resetState, setResetState] = useState(false);
  const customStyles = {
    overlay: {
       backgroundColor: 'rgba(0, 0, 0, 0.6)'
    },
    content: {
       top: '50%',
       left: '50%',
       right: 'auto',
       bottom: 'auto',
       marginRight: '-50%',
       transform: 'translate(-50%, -50%)',
       width: '50%',
    }
  }

  const handleAddToCart = product => {
    dispatch({
      type: 'add',
      payload: product
    })
  }

  const handleDecreaseCart = product => {
    dispatch({
      type: 'decrease',
      payload: product
    })
  }

  const handleCheckout = async (event) => {
    event.preventDefault();
    const pay_amount = event.target.pay_amount.value;
    const data = carts.map((cart) => {
      return {
        id: cart.id,
        quantity: cart.quantity
      };
    });

    await api.post('/transactions', {
      total_price: total,
      paid_amount: pay_amount,
      products: data
    }).then((res) => {
      if(res.data.statusCode === 200) {
        document.getElementById("form").reset();
        setTotal(total);
        setNoOrder(res.data.payload[0].no_order);
        setPayAmount(res.data.payload[0].paid_amount);
        const change = res.data.payload[0].paid_amount - total;
        setChange(change);
        setTimeout(() => {
          setIsOpen(true);
        }, 0.3);
      }
    });
  }

  const rupiah = (number)=>{
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR"
    }).format(number);
  }

  const reset = () => {
    dispatch({
      type: 'reset',
    })
  }
  
  return (
    <div className={styles.cart}>
      <div className={styles.header}>
      <h3>Cart</h3>
      <button onClick={() => reset()}>X</button>
      </div>
      
      <div className={styles['cart__cart-list']}>
        {carts.map((cart, index) => {
          return (
            <div key={index} className={styles['cart-item']}>
              <div className={styles['cart-item__image']}>
                <Image 
                  src={cart.img_product}
                  alt={cart.name}
                  fill
                  style={{ objectFit: 'contain' }}
                />
              </div>
              <div className={styles['cart-item__desc']}>
                <p>{cart.name}</p>
                <p>{cart.price}</p>
              </div>
              <div className={styles['cart-item__action']}>
                <button onClick={() => handleDecreaseCart(cart)}>-</button>
                <p>{cart.quantity}</p>
                <button onClick={() => handleAddToCart(cart)}>+</button>
              </div>

              
            </div>
          )
        })}
      </div>

      <form id="form" onSubmit={handleCheckout}>
          <h3>Total: {rupiah(total)} </h3>
      <div className={styles['cart__pay']}>
        <div>Bayar: </div>
       <input className={styles['pay__pay-input']}name="pay_amount" type="number"  id="rupiah" required autofocus></input>
      </div>

      <div classs={styles['cart__checkout']}>
        <button type="submit" className={styles['checkout__btn-checkout']}>Checkout</button>
      </div>
      <Modal isOpen={isOpen} onRequestClose={() => setIsOpen(false)} style={customStyles}>
        <h1>No Order: {noOrder} </h1>
           <div className={styles.card}>
            <div>
              <div className={styles.total}>
                <div>Total Belanja: </div>
                <div>{rupiah(totalValue)}</div>
              </div>
              <div className={styles.pay}>
                <div>Total dibayar: </div>
                <div>{rupiah(payAmount)}</div>
              </div>
              <div className={styles.cashback}>
                <div>Uang Kembalian: </div>
                <div>{rupiah(change)}</div>
              </div>

            </div>

           </div>
           <div className={styles.btn}>
            <button className={styles['close-btn']} onClick={() => {setIsOpen(false); reset();}}>Close</button>

           </div>

         </Modal>

      </form>
      
    </div>
  )
}

export default Cart;