import React, {useEffect, useState} from 'react';
import styles from './index.module.css';

import api from '@/api';
const TransactionList = () => {  
  const [datas, setDatas] = useState([]);

  useEffect(() => {
    fetchTransaction();
  }, []);

  const fetchTransaction = async () => {
    const response = await api.get('/transactions');
    setDatas(response.data.payload.transactions);
  }


  const rupiah = (number)=>{
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR"
    }).format(number);
  }


  return (
   <div>
      <div className={styles['transaction-list']}>
        {datas.map((data, index) => {
          return (
            <div key={index} className={styles['card-transaction-list']}>
         <h1>No Order: {data.no_order} </h1>
         <ul>
          <li>
            <div className={styles['wrapper-total-price']}>
              <div>Total Harga</div>
              <h1>{rupiah(data.total_price)}</h1>
            </div>
            <div className={styles['wrapper-total-pay']}>
              <div>Dibayar</div>
              <h1>{rupiah(data.paid_amount)}</h1>
            </div>
          </li>
          <li>
            <div className={styles['wrapper-item']}>
              <div>Dibayar</div>
              
              {data.products.map((product, index) => {
                return (
                  <div key={index} className={styles['item-list']}>
              <div>{product.product}</div>
              <div>{product.quantity} pcs</div>
             </div>
                )
              })}
             
             
             
            </div>
          </li>
         </ul>
        </div>
          )
        })}

        
    </div>
   </div>
  )
 }

 export default TransactionList;