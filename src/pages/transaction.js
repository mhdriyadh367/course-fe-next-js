import TransactionList from "@/components/elements/TransactionList/TransactionList";
import Layout from "@/components/layouts/Layout";

import styles from '@/styles/Home.module.css'
export default function Transaction() {
  return (
    <>
     <Layout>
        <h1>Transaction History Page</h1>

        <TransactionList />
    
     </Layout>
    </>
  )
}
